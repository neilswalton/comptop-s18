# README #

This repository is for class materials for Computational Topology, Spring 2018, taught by Prof. Fasy.

### What is in this repository? ###

The folders in this repository contain all materials for this class.

- hw: The LaTex files with the homework assignments, as well as a template for your submissions. 

### How do I contact you? ###

brittany.fasy@montana.edu
